#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include<string.h>

extern double usrFF(int nIn, int nOut,  double * pvect,char**pName,int*pCode);
/*   
The usrFF function appears as factor at squared matrix element for 
Monte Carlo calculations.  CalcHEP code has a dummy version of this function 
which always return 1.  The dummy version is replaced on the  user one if 
its code is passed to CalcHEP linker via 'Libraries' model file. One can use 
CALCHEP and  WORK  environment variables  to specify path to the code. 
These variables are  automatically defined in calchep and calchep_batch  scripts. 
Also one can use any other environment variables defined separately. 
 Parameters of usrFF:
    nIn - number of incoming particles;
    nOut- number of outgoing particles;
    pvect  presents momenta of particles: 
       4-momentum of  i^{th} particle ( i=0,1,...,nIn+nOut-1)  is 
       q[k]=pvect[4*i+k]  k=0,1,2,3; 
       q[0] - is particle energy, which always is positive.
       q[3] - specify projection of momentum on axis of collision.

    pName[i] (i=0,..nIn+nOut-1) contains name of i^th particle involved in
    reaction and pCode[i] is the corresponding PDG code. 

    Auxiliary functions which can help for construct usrFF are 
*/

extern int findval(char*name, double *value);
extern int qnumbers(char*pname, int *spin2, int * charge3, int * cdim);

/* The first one gives valueof  model parameter specified by its name.
If this  parameter indeed presented in the model then return value is zero 
and  parameter  'value' gets corresponding number. 

The qnumbers function gives  particle  quantum numbers: spin*2, 
(electric charge)*3 and dimension of color group representation. Return value
is PDG code which has to agree with pCode array data.
*/


double usrFF(int nIn, int nOut, double * pvect,char**pName,int*pCode) 
{
	// momentum transfer
	double p2[4] = {pvect[4], pvect[5], pvect[6], pvect[7]};
	double p5[4] = {pvect[16], pvect[17], pvect[18], pvect[19]};
	double dp[4] = {p2[0]-p5[0], p2[1]-p5[1], p2[2]-p5[2], p2[3]-p5[3]};
	double t = fabs(dp[0]*dp[0] - dp[1]*dp[1] - dp[2]*dp[2] - dp[3]*dp[3]);
	// electron mass
	double me = 0.511e-3; // [GeV]
	// atomic number & a
	double Z; findval("zTg", &Z);
	double a = 111.*pow(Z,-1./3.)/me;
	// nucleon number & d 
	double A; findval("aTg", &A);
	double d = 0.164*pow(A, -2./3.);
	// form factor [App-A of 0906.0580]
	double weight = pow( a*a*t / (1.+a*a*t) / (1. + t/d), 2.);
	//printf("(Z, A, t, a, d, weight) = (%f, %f, %f, %f, %f, %f)\n", Z, A, t, a, d, weight);
	return weight; 
}
